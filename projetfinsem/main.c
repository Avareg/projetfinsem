#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void chiffrement(char[]);//Pour l'algorithme global de chiffrement
void dechiffrement(char[]);//pour l'algorithme global de déchiffrement
int verifSyntaxe(char[]);//pour vérifier la syntaxe des fichiers
void affichageResultatSyntaxe(int);//pour afficher le résultat de la procédure ci-dessus
void encodage(char[]);//pour l'encodage du fichier
void decodage(char[]);//pour le décodage du fichier
void affichageFichier(char[]);//pour afficher le ficher résultat

void main(){
	char choix[1]="C";
	char code[20];
	int compteur=0;
	do{//scan de la clef "code" et vérification de sa syntaxe
		printf("Rentrez votre clef (20 caracteres maximum) ");
		scanf("%s",code);
		compteur=0;
		while(compteur<strlen(code)&&((code[compteur]>=65&&code[compteur]<=90)||(code[compteur]>=97&&code[compteur]<=122))){
			compteur++;
		}
	}while(compteur!=strlen(code));
	compteur=0;
	do{
		if(code[compteur]<97){
			code[compteur]+=32;
		}
		compteur++;
	}while(compteur<strlen(code));
	do{//coeur de l'application où on peut faire son choix de chiffrement et de déchiffrement
		printf("chiffrement : C , dechiffrement  : D , Fermer l'application : X | ");
		scanf("%s",choix);
		if (choix[0]==67){
			chiffrement(code);
		}else if(choix[0]==68){
			dechiffrement(code);
		}
	}while(choix[0]!=88);
}
void chiffrement(char code1[]){
	int resultVerifSyntaxe=verifSyntaxe("txt.decode");
	affichageResultatSyntaxe(resultVerifSyntaxe);
	if (resultVerifSyntaxe==EOF){
		encodage(code1);
		affichageFichier("txt.code");
	}
}
void dechiffrement(char code2[]){
	int resultVerifSyntaxe=verifSyntaxe("txt.code");
	affichageResultatSyntaxe(resultVerifSyntaxe);
	if (resultVerifSyntaxe==EOF){
		decodage(code2);
		affichageFichier("txt.decode");
	}
}
int verifSyntaxe(char fichier[]){
	FILE* pointFichier=NULL;
	pointFichier = fopen(fichier, "r");
	int leCaractere;
	do{
		leCaractere=fgetc(pointFichier);
	}while(((leCaractere>=65&&leCaractere<=90)||(leCaractere>=97&&leCaractere<=122)||(leCaractere<58&&leCaractere>47)||leCaractere==32||leCaractere==10)&&leCaractere!=EOF);
	fclose(pointFichier);
	return leCaractere;
}
void affichageResultatSyntaxe(int result){
	if (result==EOF){
		printf("Syntaxe OK\n");
	}else{
		printf("Syntaxe Error '%c'\n", result);
	}
}
void encodage(char code3[]){
	FILE* pointFichierSrc=NULL;
	pointFichierSrc = fopen("txt.decode", "r");
	FILE* pointFichierResult=NULL;
	pointFichierResult = fopen("txt.code", "w+");
	int compteur=0;
	int leCaractere;
	do{
		leCaractere=fgetc(pointFichierSrc);
		if (leCaractere>90){
			fputc(((leCaractere-2*97+code3[compteur])%26)+97,pointFichierResult);
			compteur++;
		}else if(leCaractere>60){
			fputc(((leCaractere-65-97+code3[compteur])%26)+65,pointFichierResult);
			compteur++;
		}else if(leCaractere!=-1){
			fputc(leCaractere,pointFichierResult);
		}
		if(compteur>=strlen(code3)){
			compteur=0;
		}
	}while(leCaractere!=EOF);
	fclose(pointFichierSrc);
	fclose(pointFichierResult);
}
void decodage(char code4[]){
	FILE* pointFichierSrc=NULL;
	pointFichierSrc = fopen("txt.code", "r");
	FILE* pointFichierResult=NULL;
	pointFichierResult = fopen("txt.decode", "w+");
	int compteur=0;
	int leCaractere;
	printf("%s \n",code4);
	do{
		leCaractere=fgetc(pointFichierSrc);
		if (leCaractere>90){
			fputc(((26+leCaractere-code4[compteur])%26)+97,pointFichierResult);
			compteur++;
		}else if(leCaractere>60){
			fputc(((26+leCaractere+32-code4[compteur])%26)+65,pointFichierResult);
			compteur++;
		}else if(leCaractere!=-1){
			fputc(leCaractere,pointFichierResult);
		}
		if(compteur>=strlen(code4)){
			compteur=0;
		}
	}while(leCaractere!=EOF);
	fclose(pointFichierSrc);
	fclose(pointFichierResult);
}
void affichageFichier(char fichier[]){
	FILE* pointFichier=NULL;
	pointFichier = fopen(fichier, "r");
	int leCaractere;
	do{
		leCaractere=fgetc(pointFichier);
		printf("%c", leCaractere);
	}while(leCaractere!=EOF);
	printf("\n");
	fclose(pointFichier);
}