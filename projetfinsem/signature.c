void chiffrement(char[]);//Pour l'algorithme global de chiffrement
void dechiffrement(char[]);//pour l'algorithme global de déchiffrement
int verifSyntaxe(char[]);//pour vérifier la syntaxe des fichiers
void affichageResultatSyntaxe(int);//pour afficher le résultat de la procédure ci-dessus
void encodage(char[]);//pour l'encodage du fichier
void decodage(char[]);//pour le décodage du fichier
void affichageFichier(char[]);//pour afficher le ficher résultat